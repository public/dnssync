package cmd

import (
	"context"
	"fmt"
	"net"
	"os"
	"strings"

	"github.com/efficientip-labs/solidserver-go-client/sdsclient"
	"github.com/spf13/cobra"
	"golang.org/x/exp/slog"
)

var log = slog.New(slog.NewTextHandler(os.Stderr))

var nameservers []string
var contact string
var apiUrl string

func join(strs []string) string {
	for i := 0; i < len(strs); i++ {
		strs[i] = fmt.Sprintf("%q", strs[i])
	}
	return strings.Join(strs, " ")
}

// bindifyCmd represents the bindify command
var bindifyCmd = &cobra.Command{
	Use:   "bindify",
	Short: "Convert an IPAM zone to a Bind zone",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		zone := args[0]

		if len(nameservers) == 0 {
			log.Error("There must be at least one nameserver defined.")
			os.Exit(1)
		}

		config := sdsclient.NewConfiguration()
		config.Servers[0].URL = apiUrl

		ddi := sdsclient.NewAPIClient(config)
		auth := context.WithValue(context.Background(), sdsclient.ContextBasicAuth, sdsclient.BasicAuth{
			UserName: os.Getenv("IPAM_USERNAME"),
			Password: os.Getenv("IPAM_PASSWORD"),
		})

		// List zone records
		rrsets, _, err := ddi.DnsApi.DnsRrList(auth).Where(fmt.Sprintf("zone_name='%s'", zone)).Orderby("rr_full_name").Limit(10000).Execute()
		if err.Error() != "" {
			log.Error("failed to list records", err, "zone", zone)
			os.Exit(1)
		}

		wroteNS := false

		for _, rrset := range *rrsets.Data {
			switch *rrset.RrType {
			case "NS":
				if *rrset.RrFullName == zone {
					// We replace the NS record with our own.
					if !wroteNS {
						for _, nameserver := range nameservers {
							fmt.Printf("%s. %s IN %s %s.\n", *rrset.RrFullName, *rrset.RrTtl, *rrset.RrType, nameserver)
						}

						wroteNS = true
					}
				} else {
					fmt.Printf("%s. %s IN %s %s.\n", *rrset.RrFullName, *rrset.RrTtl, *rrset.RrType, *rrset.RrValue1)
				}
			case "CNAME":
				fmt.Printf("%s. %s IN %s %s.\n", *rrset.RrFullName, *rrset.RrTtl, *rrset.RrType, *rrset.RrValue1)
			case "SOA":
				// We replace the nameserver and contact name in the SOA.
				fmt.Printf("%s. %s IN %s %s. %s. %s %s %s %s %s\n", *rrset.RrFullName, *rrset.RrTtl, *rrset.RrType, nameservers[0], strings.ReplaceAll(contact, "@", "."), *rrset.RrValue3, *rrset.RrValue4, *rrset.RrValue5, *rrset.RrValue6, *rrset.RrValue7)
			case "TXT":
				fmt.Printf("%s. %s IN %s %s\n", *rrset.RrFullName, *rrset.RrTtl, *rrset.RrType, join(strings.Split(*rrset.RrValue1, "\n")))
			case "MX":
				fmt.Printf("%s. %s IN %s %s %s.\n", *rrset.RrFullName, *rrset.RrTtl, *rrset.RrType, *rrset.RrValue1, *rrset.RrValue2)
			case "SRV":
				fmt.Printf("%s. %s IN %s %s %s %s %s.\n", *rrset.RrFullName, *rrset.RrTtl, *rrset.RrType, *rrset.RrValue1, *rrset.RrValue2, *rrset.RrValue3, *rrset.RrValue4)
			case "AAAA":
				ip := net.ParseIP(*rrset.RrValue1)
				fmt.Printf("%s. %s IN %s %s\n", *rrset.RrFullName, *rrset.RrTtl, *rrset.RrType, ip.String())
			default:
				fmt.Printf("%s. %s IN %s %s\n", *rrset.RrFullName, *rrset.RrTtl, *rrset.RrType, *rrset.RrValue1)
			}

		}
	},
}

func init() {
	rootCmd.AddCommand(bindifyCmd)

	bindifyCmd.Flags().StringVar(&apiUrl, "api-url", "https://ipam.private.uwaterloo.ca/api/v2.0", "Base URL for the IPAM system.")

	bindifyCmd.Flags().StringSliceVar(&nameservers, "nameservers", []string{"ext-dns1.csclub.uwaterloo.ca", "ext-dns2.csclub.uwaterloo.ca"}, "Nameservers for this zone.")
	bindifyCmd.Flags().StringVar(&contact, "contact", "systems-committee@csclub.uwaterloo.ca", "Contact for the SOA record.")
}
