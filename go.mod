module git.csclub.uwaterloo.ca/public/dnssync

go 1.23

require (
	github.com/efficientip-labs/solidserver-go-client/sdsclient v0.0.0-20211104144013-8c21394c8e32
	github.com/spf13/cobra v1.6.1
	golang.org/x/exp v0.0.0-20230315142452-642cacee5cc0
)

require (
	github.com/go-logr/logr v1.2.0 // indirect
	github.com/golang/protobuf v1.2.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/net v0.0.0-20190108225652-1e06a53dbb7e // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45 // indirect
	google.golang.org/appengine v1.4.0 // indirect
	k8s.io/klog v1.0.0 // indirect
	k8s.io/klog/v2 v2.90.0 // indirect
)
